---
slug: rivercitypridefest2022
title: Peoria Proud's RiverCity PrideFest 2022
authors: [aaron]
tags: [lgbtq, lgbtqia+, pride, peoria, community]
---
# This is ignored for some reason

## When
Saturday, July 16, 2022, 10:00 AM -  8:00 PM

## Where
Peoria's Riverfront 200 Northeast Water Street Peoria, IL, 61602 United States

## Event Link
https://www.peoriaproud.org/rcp
