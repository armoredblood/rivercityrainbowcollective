---
sidebar_position: 1
---

# Central Illinois Friends

Central Illinois Friends provides a community for individuals living with HIV, as well as transportation, rent, utilities, and emergency services. We also provide free & confidential HIV and STI Screenings, HPV vaccinations, PrEP, education, and/or referral services.

## Website
[Friends of Central Illinois](https://www.friendsofcentralillinois.org/)

## Services
They can test for: HIV, Gonorrhea, Chlamydia, Hepatitis A, B, and C, Herpes 1&2, Syphilis, and Trichomoniasis. Pregnancy testing is also available