import * as cdk from 'aws-cdk-lib';
import * as path from 'path';

import { Construct } from 'constructs';
import { Ec2Action } from 'aws-cdk-lib/aws-cloudwatch-actions';
// import * as sqs from 'aws-cdk-lib/aws-sqs';

export class DiscordBotStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const ec2 = cdk.aws_ec2;

    // create vpc
    const vpc = new cdk.aws_ec2.Vpc(this, 'Vpc', {
      cidr: '10.0.0.0/16',
    });

    // upload python app folder to s3
    const asset = new cdk.aws_s3_assets.Asset(this, 'pythonAsset', {
      path: path.join(__dirname, 'python'),
    });

    // upload setup script to s3
    const setupScript = new cdk.aws_s3_assets.Asset(this, 'setupScript', {
      path: path.join(__dirname, 'setup.sh'),
    });

    // setup the instance security group
    const instanceSecurityGroup = new ec2.SecurityGroup(this, 'instanceSG', {
      vpc,
      description: 'allow traffic only on certain ports',
      allowAllOutbound: true
    })

    // add specific ingress rules to the security group
    instanceSecurityGroup.addIngressRule(ec2.Peer.anyIpv4(), ec2.Port.tcp(443), 'allow traffic from everyone on this port')

    // const rootVolume: cdk.aws_ec2.BlockDevice = {
    //   deviceName: '/dev/xvda',
    //   volume: ec2.BlockDeviceVolume.ebs(20)
    // }

    // define the ec2 instance the python app will run on
    const instance = new cdk.aws_ec2.Instance(this, 'Instance', {
      vpc: vpc,
      securityGroup: instanceSecurityGroup,
      instanceType: new ec2.InstanceType('t2.micro'),
      machineImage: cdk.aws_ec2.MachineImage.latestAmazonLinux(),
      userDataCausesReplacement: true
      // blockDevices: [rootVolume]
    });

    // setup elastic ip address (static ip)
    const elasticIp = new ec2.CfnEIP(this, 'EIP', {
      domain: 'vpc',
      instanceId: instance.instanceId
    })
    const cfnVPC = vpc.node.defaultChild as cdk.aws_ec2.CfnVPC;
    elasticIp.addDependsOn(cfnVPC)

    // grant ec2 instance access to python app and setup script
    asset.grantRead(instance.role)
    setupScript.grantRead(instance.role)

    // download python app to the ec2 instance
    const assetPath = instance.userData.addS3DownloadCommand({
      bucket: asset.bucket,
      bucketKey: asset.s3ObjectKey
    })

    // download setup script to the ec2 instance
    const setupScriptPath = instance.userData.addS3DownloadCommand({
      bucket: setupScript.bucket,
      bucketKey: setupScript.s3ObjectKey
    })

    // execute the setup script on the ec2 instance
    instance.userData.addExecuteFileCommand({
      filePath: setupScriptPath,
      arguments: `${"argument1"}`
    })





  }
}
